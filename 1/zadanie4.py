#
#Wynik działania skryptu znajduje się w pliku results4.txt
#

import json
import glob
import os
import re
from dateutil.parser import parse


resultsDir='/home/marcin/Documents/PJN/'
dataPath = '/home/marcin/Documents/PJN/data/json'
os.chdir(dataPath)
files = glob.glob('judgments*.json')
pattern=r'\bszkod(ą|ę|om?|y|zie|ach|ami)|szkód\b'
prog = re.compile(pattern, re.IGNORECASE)
counter=0
ids = []
for file in files:
    data = json.load(open(file))
    for item in data['items']:
        date = parse(item['judgmentDate'])
        if date.year == 2011:
            match = prog.search(item['textContent'])
            if match:
                counter+=1
results = open(resultsDir+"results4.txt","w+")
results.write(str(counter))
results.close()









